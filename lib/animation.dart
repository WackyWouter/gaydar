import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter_speedometer/flutter_speedometer.dart';

// Animation function for the speedometer
class RunSpeedoMeter extends ImplicitlyAnimatedWidget {
  final int percentage;

  RunSpeedoMeter({
    Key key,
    @required this.percentage,
    @required Duration duration,
    Curve curve = Curves.linear,
  }) : super(duration: duration, curve: curve, key: key);

  @override
  ImplicitlyAnimatedWidgetState<ImplicitlyAnimatedWidget> createState() {
    return _AnimatedSpeedoMeterState();
  }
}

class _AnimatedSpeedoMeterState
    extends AnimatedWidgetBaseState<RunSpeedoMeter> {
  IntTween _intCount;

  @override
  Widget build(BuildContext context) {
    return new Speedometer(
      size: 300,
      minValue: 0,
      maxValue: 100,
      currentValue: _intCount.evaluate(animation),
      warningValue: 100,
      backgroundColor: Color(0x00000000),
      meterColor: Colors.lightBlueAccent,
      warningColor: Color(0x00000000),
      displayNumericStyle:
          TextStyle(fontFamily: 'Russo', color: Colors.white, fontSize: 30),
    );
  }

  @override
  void forEachTween(TweenVisitor visitor) {
    _intCount = visitor(_intCount, widget.percentage,
        (dynamic value) => new IntTween(begin: value));
  }
}

class AnimatePercentageText extends ImplicitlyAnimatedWidget {
  final int percentage;

  AnimatePercentageText({
    Key key,
    @required this.percentage,
    @required Duration duration,
    Curve curve = Curves.linear,
  }) : super(duration: duration, curve: curve, key: key);

  @override
  ImplicitlyAnimatedWidgetState<ImplicitlyAnimatedWidget> createState() =>
      _AnimatePercentageTextState();
}

class _AnimatePercentageTextState
    extends AnimatedWidgetBaseState<AnimatePercentageText> {
  IntTween _intCount;

  @override
  Widget build(BuildContext context) {
    return new RichText(
      text: TextSpan(
        text: "You are " + _intCount.evaluate(animation).toString() + "% gay!",
        style:
            TextStyle(color: Colors.white, fontFamily: 'Russo', fontSize: 30.0),
      ),
    );
  }

  @override
  void forEachTween(TweenVisitor visitor) {
    _intCount = visitor(_intCount, widget.percentage,
        (dynamic value) => new IntTween(begin: value));
  }
}
