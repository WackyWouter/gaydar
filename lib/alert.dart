import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class Popup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      onPressed: () => infoPopup(context),
    );
  }

  infoPopup(context) {
    AlertStyle infoStyle = AlertStyle(
        animationType: AnimationType.fromTop,
        isCloseButton: false,
        isOverlayTapDismiss: false,
        animationDuration: Duration(milliseconds: 400),
        alertBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
          side: BorderSide(
            color: Colors.grey,
          ),
        ),
        titleStyle: TextStyle(
            color: Colors.deepPurple, fontFamily: "Russo", fontSize: 30),
        constraints: BoxConstraints.expand(width: 450));
    // Alert dialog using custom content
    Alert(
      context: context,
      style: infoStyle,
      title: "Gay-O-Meter",
      content: Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text:
                  "Find out how gay you are! \n \n This was made as a gag gift for Stephan Sie. \n Happy Birthday Stephan!",
              style: TextStyle(
                  color: Colors.black, fontSize: 20, fontFamily: 'Russo'),
            ),
          ),
        ),
      ),
      buttons: [
        DialogButton(
          onPressed: () => Navigator.pop(context),
          child: Text(
            "Close",
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontFamily: 'Russo'),
          ),
          color: Colors.deepPurple,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
  }

  gayAlert(context) {
    AlertStyle infoStyle = AlertStyle(
        animationType: AnimationType.fromTop,
        isCloseButton: false,
        isOverlayTapDismiss: false,
        animationDuration: Duration(milliseconds: 400),
        alertBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
          side: BorderSide(
            color: Colors.grey,
          ),
        ),
        titleStyle: TextStyle(
            color: Colors.deepPurple, fontFamily: "Russo", fontSize: 30),
        constraints: BoxConstraints.expand(width: 450));
    // Alert dialog using custom content

    Alert(
      context: context,
      style: infoStyle,
      title: "HAAAA GAYYYY!!",
      content: Container(
          alignment: Alignment.center,
          child: Image(image: AssetImage("assets/hagay.gif"))),
      buttons: [
        DialogButton(
          onPressed: () => Navigator.pop(context),
          child: Text(
            "Close",
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontFamily: 'Russo'),
          ),
          color: Colors.deepPurple,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
  }
}
