import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'animation.dart';
import 'alert.dart';
import 'dart:async';
import 'dart:math';

// TODO make an app icon
// https://stackoverflow.com/questions/43928702/how-to-change-the-application-launcher-icon-on-flutter

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gay-O-Meter',
      home: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/background.png"), fit: BoxFit.cover)),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
              elevation: 0,
              backgroundColor: Colors.transparent,
              centerTitle: true,
              leading: Popup()),
          body: MyHomePage(),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  int percentageGoal = 0;
  bool reset = false;
  bool haGay = false;
  String btnText = "Press me";

  AnimationController _controller;
  Animation<int> _animation;
  Timer _timer;

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Flexible(
          flex: 1,
          child: Center(
            child: RichText(
              text: TextSpan(
                text: "Gay-O-Meter",
                style: TextStyle(
                    color: Colors.white, fontFamily: 'Russo', fontSize: 30.0),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(40.0),
          child: RunSpeedoMeter(
              percentage: percentageGoal, duration: Duration(seconds: 3)),
        ),
        Flexible(
          flex: 1,
          child: Center(
              child: AnimatePercentageText(
                  percentage: percentageGoal, duration: Duration(seconds: 3))),
        ),
        Flexible(
          flex: 1,
          child: OutlineButton(
            onPressed: () {
              reset = !reset;
              changeBtn(reset);
            },
            borderSide: BorderSide(color: Colors.white, width: 5),
            highlightedBorderColor: Colors.lightBlueAccent,
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                btnText,
                style: TextStyle(
                    fontSize: 30, fontFamily: 'Russo', color: Colors.white),
              ),
            ),
          ),
        )
      ],
    );
  }

  void changeBtn(bool reset) {
    setState(() {
      // if button is pressed again with hagay on cancel the popup
      if (haGay) {
        _timer.cancel();
        haGay = false;
      }

      if (reset) {
        btnText = "Reset";
        percentageGoal = Random().nextInt(100) + 1;
      } else {
        btnText = "Press me";
        percentageGoal = 0;
      }

      // hagay = true and show hagay popup after delay
      if (percentageGoal == 100) {
        haGay = true;
        _timer = Timer(Duration(seconds: 3, milliseconds: 500), () {
          Popup().gayAlert(context);
        });
      }
    });
  }
}
